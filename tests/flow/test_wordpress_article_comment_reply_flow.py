import time
from base64 import b64encode
import requests
import lorem


post_created_id = 0
first_comment_for_post_created_id = 0


base_url = 'https://gaworski.net'
post_create = base_url + "/wp-json/wp/v2/posts"
comment_create = base_url + "/wp-json/wp/v2/comments"


user_editor_id = 2
user_editor_name = 'editor'
user_editor_password = 'HWZg hZIP jEfK XCDE V9WM PQ3t'
editor_token = b64encode(f"{user_editor_name}:{user_editor_password}".encode('utf-8')).decode("ascii")


user_commenter_id = 3
user_commenter_name = 'commenter'
user_commenter_password = 'SXlx hpon SR7k issV W2in zdTb'
commenter_token = b64encode(f"{user_commenter_name}:{user_commenter_password}".encode('utf-8')).decode("ascii")


def test_create_post():
    global post_created_id
    payload = {
        'title': 'This is new post ' + str(int(time.time())),
        'content': lorem.paragraph(),
        'excerpt': lorem.sentence(),
        'status': 'publish',
    }
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + editor_token,
    }
    response = requests.post(post_create, headers=headers, json=payload)

    assert response.status_code == 201
    assert response.reason == "Created"
    assert response.json()["author"] == user_editor_id

    post_created_id = response.json()["id"]


def test_create_comment():
    global post_created_id, first_comment_for_post_created_id
    payload = {
        "post": post_created_id,
        "status": 'approve',
        "content": lorem.paragraph(),
    }
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + commenter_token,
    }
    response = requests.post(comment_create, headers=headers, json=payload)


def test_create_reply_for_comment():
    global post_created_id, first_comment_for_post_created_id

    payload = {
        'post': post_created_id,
        'parent': first_comment_for_post_created_id,
        'status': 'approve',
        'content': lorem.paragraph(),
    }
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + editor_token,
    }
    response = requests.post(comment_create, headers=headers, json=payload)

    assert response.status_code == 201
    assert response.reason == "Created"
    assert response.json()["parent"] == first_comment_for_post_created_id
    assert response.json()["post"] == post_created_id
    assert response.json()["author"] == user_editor_id
